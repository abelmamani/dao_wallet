from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, create_engine, MetaData, Table, select
from config_vars import BBDD_CONNECTION

Base = declarative_base()


class Countries(Base):
    __tablename__ = "countries"
    print("entering parameters config")
    engine = create_engine(BBDD_CONNECTION)
    metadata = MetaData()
    cou = Table("countries", metadata, autoload=True, autoload_with=engine, schema='billetera_abel')
    id_not_in_db = Column(Integer, primary_key=True)
    print("finished config for parameters")
    
    @classmethod
    def single_countries(cls, *, cou_id):
        """
        cual es countrie con id 
        """
        query = select([cls.cou]).where(cls.cou.c.cou_id == cou_id)
        return query
        
    @classmethod
    def all_countries(cls):
        """
        Cuáles son todas las countries
        """
        query = select([cls.cou])
        return query